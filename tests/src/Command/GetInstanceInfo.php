<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Test\Command;

use Spinit\Dev\MRoute\Core\Command;

/**
 * Description of GetInstanceInfo
 *
 * @author ermanno
 */
class GetInstanceInfo extends Command {
    //put your code here
    public function exec($param = null) {
        list($schema, $url) = explode('://', (string) $param->getUri()); 
        $info = $this->getMain()->getDataSource()->getAdapter()->getInstanceMap($url, $this->getMain());
        return $info;
    }

}
