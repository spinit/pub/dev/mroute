<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Test\Manager;

use Spinit\Dev\MRoute\Instance;
use Spinit\Lib\Model\Model;
use Spinit\Lib\Model\Adapter\Xml\ModelAdapterXml;

/**
 * Description of ManagerGrid
 *
 * @author ermanno
 */

class ManagerAccessPoint extends Instance {
    private static $admin = null;
    
    public function start() {
        return __CLASS__;
    }
    public function install($admin = null) {
        $model = new Model(new ModelAdapterXml('Admin', __DIR__.'/Model/Admin.xml'), $this->getDataSource());
        $model->set('name', $admin['name']);
        $model->save();
        self::$admin = $model;
    }
    
    public static function getAdmin() {
        return self::$admin;
    }
}
