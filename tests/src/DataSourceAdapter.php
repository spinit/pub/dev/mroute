<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Test;

use Spinit\Lib\DataSource\AdapterInterface;
use Webmozart\Assert\Assert;
use Spinit\UUIDO;
use Spinit\Util;
/**
 * Description of DataSourceAdapter
 *
 * @author ermanno
 */
class DataSourceAdapter implements AdapterInterface {
    
    private $routes;
    private $c;
    use Util\ParamTrait;
    
    public function __construct($proto) {
        $this->proto = $proto;
        $this->routes = [];
    }
    public function getType() {
        return $this->proto;
    }
    public function setInstanceMap($route, $class) {
        $this->routes[$route] = ['manager' => $class];
    }
    public function getInstanceMap($url, $main) {
        $found = false;
        foreach($this->routes as $route=>$class) {
            if (substr($url, 0, strlen($route)) == $route) {
                if ($found === false or strlen($found) < strlen($route)) {
                    $found = $route;
                }
            }
        }
        Assert::notEmpty($found);
        return $this->routes[$found];
    }
    public function getTypeList() {
        return [$this->getType()];
    }
    public function getPkeyDefault() {
        return 'id';
    }
    public function check($name) {
        return false;
    }
    public function setCounter(UUIDO\Maker $counter) {
        $args = func_get_args();
        $this->c = array_shift($args);
    }
    public function getCounter() {
        return $this->c;
    }
    public function insert() {
        
    }
    public function align() {
        
    }
    public function normalize()
    {
        
    }
}
