<?php

namespace Spinit\Dev\MRoute\UnitTest;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\MRoute\Main;
use Spinit\Util;
use Spinit\Lib\DataSource\DataSource;
use Spinit\Lib\DataSource\AdapterManager;
use Spinit\Dev\MRoute\Core\InstanceRegister;

use Spinit\Dev\MRoute\Test\DataSourceAdapter;
use Spinit\Dev\MRoute\Test\Manager\ManagerGrid;
use Spinit\Dev\MRoute\Test\Manager\ManagerSite;

use Spinit\Dev\MRoute\Instance\InstallationInstance;

use GuzzleHttp\Psr7\ServerRequest;

/**
 * Description of RouteTest
 *
 * @author ermanno
 */


class MainTest extends TestCase {
    
    
    public function testMatch() {
        $aa = json_encode(['uno'=>'#1:20', 'pre'=>'oooo']);
        \preg_match_all('/"(#[0-9]+:[0-9]+)"/', $aa, $match);
        foreach($match[0] as $k=>$v) {
            $aa = str_replace($v, $match[1][$k], $aa);
        }
        $this->assertStringContainsString('ooo"', $aa);
        $this->assertStringContainsString('#1:20,', $aa);
    }
    
    public function testDataSourceAdapter() {
        $this->assertInstanceOf(DataSourceAdapter::class, AdapterManager::getAdapter("test:prova"));
    }
    
    public function testMain() {
        
        $ds = new DataSource("test:prova");
        // registrazione dei manager da poter utilizzare
        $ds->getAdapter()->setInstanceMap('site', 'Spinit:Dev:MRoute:Test:Manager:ManagerGrid');
        $ds->getAdapter()->setInstanceMap('site/prova', 'Spinit:Dev:MRoute:Test:Manager:ManagerSite');
        
        $this->assertEquals('Spinit:Dev:MRoute:Test:Manager:ManagerGrid', Util\getColonsPath(ManagerGrid::class));
        
        $router = new Main($ds);
        
        $manager = $router->getInstance(new ServerRequest('GET', 'http://site/test?message=OK'));
        $this->assertInstanceOf(ManagerGrid::class, $manager);
        $response = $manager->start();
        $this->assertEquals('GRID Instance', $response);
        
        $manager = $router->getInstance(new ServerRequest('GET', 'http://site/prova?message=OK'));
        $this->assertInstanceOf(ManagerSite::class, $manager);
        $response = $manager->start();
        $this->assertEquals('SITE Instance', $response);
    }
    
    public function testInstallMessage() {
        
        $ds = new DataSource("test-inst:prova2");
        $router = new Main($ds);
        
        $manager = $router->getInstance(new ServerRequest('GET', 'http://site/test?message=OK-TEST'));
        $this->assertInstanceOf(InstallationInstance\Manager::class, $manager);
        $response = $manager->run();
        $this->assertEquals('OK-TEST', $response->toString());
    }
    
    
    public function testInstallForm() {
        
        $ds = new DataSource("test-inst:prova2");
        $router = new Main($ds);
        
        $manager = $router->getInstance(new ServerRequest('GET', 'http://site/'));
        $this->assertInstanceOf(InstallationInstance\Manager::class, $manager);
        $response = $manager->run();
        $this->assertStringContainsString('id="user[name]"', $response->toString());
        
    }
    
    public function testInstallFormPassword() {
        $ds = new DataSource("test-inst:prova2");
        $router = new Main($ds);
        $post = [
            '_'=>['osy'=>['event'=>'start']],
            'con_str' => 'test-inst:prova3',
            'acp' => [],
            'user'=> ['name'=>'admin', 'passwd'=>'uno', 'passwd2'=>'due']
        ];
        $request = new ServerRequest('POST', 'http://site/', ['Content-Type'=>'application/json'], json_encode($post));
        
        $this->assertEquals('application/json', $request->getHeaderLine('Content-Type'));
        $this->assertEquals($post, json_decode($request->getBody()->getContents(), 1));
        
        $manager = $router->getInstance($request);
        $response = $manager->run();
        $this->assertEquals('error', $response['status']);
        $this->assertEquals('passwd-miss-match', $response['code']);
        
        $post = [
            '_'=>['osy'=>['event'=>'start']],
            'con_str' => 'test-inst:prova3',
            'acp' => [],
            'user'=> ['name'=>'admin', 'passwd'=>'uno', 'passwd2'=>'uno']
        ];
        $request = new ServerRequest('POST', 'http://site/', ['Content-Type'=>'application/json'], json_encode($post));
        $manager = $router->getInstance($request);
        $response = $manager->run();
        $this->assertEquals('success', $response['status']);
    }
    
    
    public function xxtestInstallAccesssPoint() {
        $ds = new DataSource("test-inst:prova2");
        $router = new Main($ds);
        $class = 'Spinit:Dev:MRoute:Test:Manager:ManagerAccessPoint';
        $post = [
            '_'=>['osy'=>['event'=>'start']],
            'con_str' => 'test-inst:prova3',
            'acp' => [$class =>'localhost/test'],
            'user'=> ['name'=>'admin', 'passwd'=>'uno', 'passwd2'=>'uno']
        ];
        Util\getEnv('STOP', 1);
        $request = new ServerRequest('POST', 'http://site/', ['Content-Type'=>'application/json'], json_encode($post));
        $manager = $router->getInstance($request);
        $response = $manager->run();
        if ($response['trace']) {
            debug($response->toString());
        }
        debug($response->toString());
        $this->assertEquals('success', $response['status']);
        // ora si ricrea un oggetto diella classe testata ... e si dovrebbe avere l'utente installato
        $ap = Util\getInstance($class, $router);
        $this->assertEquals('admin', $ap::getAdmin()->get('name'));
    }
    
}
