<?php

include dirname(__DIR__).'/autoload.php';

use Spinit\Dev\MRoute\Main;
use Spinit\Dev\MRoute\Core\InstanceRegister;

use Spinit\Util;
use GuzzleHttp\Psr7\ServerRequest;
use Spinit\Lib\DataSource\DataSource;

InstanceRegister::setInstanceManager('Spinit:Dev:MRoute:Test:ManagerGrid', 'Grid Test', 1, 1);
InstanceRegister::setInstanceManager('Spinit:Dev:MRoute:Test:ManagerSite', 'Site Test', 0, 0);

$ds = new DataSource(Util\getenv('CONNECTION_STRING'));
$app = new Main($ds);
echo $app->getInstance(ServerRequest::fromGlobals())->run();

