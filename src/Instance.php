<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute;

use Spinit\Util;
use GuzzleHttp\Psr7\Request;
use Spinit\Dev\MRoute\Core\Type\InstanceInterface;
use Spinit\Dev\MRoute\Core\Type\MainInterface;
use Spinit\Dev\MRoute\Core\Response;
use Spinit\Dev\MRoute\Core\Exception\ResponseException;
use Spinit\UUIDO;
use Spinit\Lib\DataSource;
use Webmozart\Assert\Assert;
use Spinit\Util\Error\NotFoundException;
use Spinit\Dev\MRoute\Core\Formatter;

use function Spinit\Util\nvl;

/**
 * Description of Manager
 *
 * @author ermanno
 */
abstract class Instance implements InstanceInterface {
    
    use Util\TriggerTrait;
    use Util\ParamTrait;
    use DataSource\GetDataSourceTrait {
        getDataSource as __getDataSourceTrait;
    }
    /**
     *
     * @var Main
     */
    private $main;
    private $request;
    private $response;
    private $env = null;

    private $counter;
    
    protected $info;
    private $user = false;
    private $console = null;
    private $lngList;
    
    public function __construct(MainInterface $main, $info = null)
    {
        $this->main = $main;
        $this->dsList = [];
        $this->setDataSource($main->getDataSource(), 'main');
        $this->setInfo($info);
        $this->setResponse(new Response());
        $this->bindExec('flush', function() {
            foreach($this->getDataSourceList() as $ds) {
                $ds->getAdapter()->trigger('flush');
            }
        });
        // se l'istanza viene trovata allora viene emesso questo evento
        $this->bindExec('found', function() {
            try {
                $this->loadLanguages();
                $this->loadEnvironment();
            } catch (\Exception $e) {

            }
        });
        $this->registerEvent();
    }
    private function loadLanguages() {
        $this->lngList = [];
        foreach($this->getDataSource()->query("
        select l.cod, hex(l.id) as id, l.als
        from osy_itm l 
        inner join osy_itm lt on (l.id_typ = lt.id and 
                                    lt.urn = 'urn:opensymap.org@type#lang')") as $rec) {
            $this->lngList[$rec['cod']] = $rec['id'];
            if ($rec['als']) {
                $this->lngList[$rec['als']] = $rec['id'];
            }
        }
    }
    private function loadEnvironment() {
        if (!$this->getInfo('id')) {
            return;
        }
        $cmd = "
		select *
		from (
			select 0 as oo, nme, val 
			from osy_env 
			where id_url is null 
			union
			select case when ee.id_url = uu.id_par then 1 else 2 end, nme, val 
            from @DataSource.main@.osx_ice_url uu
            inner join osy_env ee on (ee.id_url = uu.id or ee.id_url = uu.id_par) 
            where uu.id = {{@id_url}}
        ) d 
        order by d.oo
        ";
        $list = $this->getDataSource()->query($this->normalizeQuery($cmd), $this->getInfo());
        $this->env = [];
        foreach($list as $rec) {
            $this->env[$rec['nme']] = $rec['val'];
        }
    }

    public function getLang() {
        $args = func_get_args();
        $ret = null;
        if (!count($args)) {
            if (count($this->lngList)) {
                $ret = ['id'=>reset($this->lngList), 'name'=>key($this->lngList)];
            }
            return $ret;
        }
        // altrimenti
        $lng = array_shift($args);
        $id = Util\arrayGet($this->lngList, $lng);
        $arLng = Util\asArray($lng, '-');
        if (!$id and count($arLng) > 1) {
            $id = Util\arrayGet($this->lngList, $arLng[0]);
            $lng = $arLng[0];
        }
        if (!$id and in_array($lng, $this->lngList)) {
            $id = $lng;
            $lng = array_search($lng, $this->lngList);
        }
        if ($id) {
            $ret = ['id'=>$id, 'name'=>$lng];
        }
        return $ret;
    }

    public function getEnv() {
        $args = func_get_args();
        array_unshift($args, $this->env);
        return call_user_func_array('\\Spinit\\Util\\arrayGet', $args);
    }

    protected function registerEvent() {}
    
    protected function setInfo($info) {
        $this->info = $info;
        $this->counter = '';
        if ($this->getInfo('id', '')) {
            try {
                $this->counter = new UUIDO\Maker($this->getInfo('id'));
            }catch (\Exception $e) {
                debug($this->getInfo());
            }
        }
        // se è impostata una connessione particolare ... allora viene caricata sul Datasource di default
        if ($con_str = $this->getInfo('con_str', '')) {
            $this->setDataSource($con_str);
        } else {
            $this->setDataSource($this->getMain()->getDataSource());
        }
        return $this;
    }
    public function getConsole() {
        if (!$this->console)  {
            $this->console = new Console($this->getHome());
        }
        if ($this->console->getRoot() != $this->getHome()) {
            debug($this->console->getRoot(), $this->getHome(), $this->getInfo());
        }
        return $this->console;
    }
    public function getHome($path = '') {
        return fspath($this->getMain()->getHome(), nvl($this->getInfo('home'), $this->getInfo('id')), $path);
    }
    public function setResponse(Response $response)
    {
        $this->response = $response;
        return $this;
    }
    
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * 
     * @return Main
     */
    public function getMain() {
        return $this->main;
    }
    
    public function getInfo() {
        $args = func_get_args();
        if (!count($args)) {
            return $this->info;
        }
        array_unshift($args, $this->info);
        return call_user_func_array("\\Spinit\\Util\\arrayGet", $args);
    }

    public function makePath() {
        $args = func_get_args();
        $part = array_filter(
            array_map(function($item) {
                return trim($item, '/');
            }, $args), 
            'strlen');
        //if (count($part))    debug($part);
        $end = '';
        if (substr($args[func_num_args()-1], -1) == '/') {
            $end = '/';
        }
        $url = ltrim(implode('/', $part).$end, '/');
        $scheme = $this->getRequest()->getUri()->getScheme();
        return $scheme.'://'.trim($this->getInfo('url'), '/').'/'.$url;
    }
    
    /**
     * Se si passa una funzione di caricamento utente (come ultimo parametro) ... viene usata
     * per impostare l'utente nel caso non lo fosse già. Altrimenti viene caricato dai cookie.
     * Gli altri parametri possono essere usati per prelevare il valore di interesse
     * @return type
     * @throws NotFoundException
     */
    public function getUser() {
        $args = func_get_args();
        $loadToken = null;
        if (count($args)) {
            $loadToken = is_callable($args[count($args)-1]) ? array_pop($args) : null;
        }
        
        $loadDefault = function() {
            return Util\arrayGet($this->getRequest()->getCookieParams(), 'token-user');
        };
        
        if (!$this->user and $token = call_user_func($loadToken?:$loadDefault)) {
            $this->user = json_decode($this->getMain()->decrypt($token), 1);
        }
        if (!$this->user) {
            throw new NotFoundException("Accesso non effetuato");
        } else if (is_array($this->user)) {
            $this->user = new Util\DictionaryBase($this->user);
        }
        
        return count($args) ? call_user_func_array([$this->user, 'get'], $args) : $this->user;
    }
    
    public function setUser($user) {
        $args = func_get_args();
        $registerToken = is_callable($args[count($args)-1]) ? array_pop($args) : null;
        $registerDefault = function($token) {
            $ret = setcookie('token-user', $token, ['expires'=>0, 'path'=>$this->getPathRoot(), 'samesite' => 'Lax']);
            if (!$ret ) {
                $ret = setcookie('token-user', $token, 0, $this->getPathRoot());
            }
        };
        if (!$user) {
            call_user_func($registerToken?:$registerDefault, '');
            $this->user = null;
        } else {
            if (is_array($user)) {
                $this->user = new Util\DictionaryBase($user);
            } else if ($user instanceof Util\DictionaryBase) {
                $this->user = new $user;
            } else {
                Assert::count($args, 2, 'Chiamata errata metodo '.__METHOD__);
                Assert::notNull($this->user);
                call_user_func_array([$this->user, 'set'], $args);
            }
            $data = json_encode($this->user->asArray());
            call_user_func($registerToken?:$registerDefault, $this->getMain()->encrypt($data));
        }
        return $this;
    }
    
    public function hasUser() {
        try {
            $this->getUser();
        } catch (\Exception $ex) {
            return false;
        }
        return true;
    }
    public function getDataSource($name = '')
    {
        $ds = $this->__getDataSourceTrait($name);
        if ($this->counter) {
            $ds->setCounter($this->counter);
        }
        try {
            $ds->getAdapter()->setParam('userTrace', $this->getUser('id'));
        } catch (\Exception $e) {
            
        }
        return $ds;
    }
    /*
    public function setDataSource($name, $ds)
    {
        $this->dsList[$name] = $ds;
        return $ds;
    }
    public function reconnect () {
        foreach($this->dsList as $ds) {
            $ds->reconnect();
        }
    }
     * 
     */
    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }
    public function getRequest()
    {
        Assert::notNull($this->request, 'Request non impostata');
        return $this->request;
    }
    public function getRequestUri()
    {
        list($schema, $uri,) = explode('://', $this->getRequest()->getUri());
        return $uri;
    }
    
    public function getPath() {
        return ltrim(substr($this->getRequestUri(), strlen($this->getInfo('url'))), '/');
    }
    public function getPathRoot() {
        $part = parse_url('//'.$this->getInfo('url'));
        return Util\arrayGet($part, 'path');
    }
    
    abstract protected function start ();
    
    /**
     * Inizializza tutte le applicazioni che sono state registrate
     */
    public function init() {
        return $this->install(null);
    }
    
    public function install($admin) {
        
    }
    
    /**
     * Gestione dei comandi tramite factory
     * @param type $name
     * @return type
     */
    public function getCommand($name) {
        $args = func_get_args();
        $name = array_shift($args);
        array_unshift($args, $name);
        array_unshift($args, getArch($this));
        if ($name[0] != ':') {
            try {
                $factory = Util\FactoryMap::get($args[0], Util\upName(get_class($this)).':CommandFactory');
                return call_user_func_array([$factory, 'get'], $args);
            } catch (\Exception $e) {
                // altrimenti prosegue richiedendo la classe indicata
                $args[1] = Util\upName(get_class($this)).':Command:'.$name;
            }
        }
        return call_user_func_array(Util\FactoryMap::class.'::get', $args);
    }
    
    final public function run($throwException = true) {
        try {
            return $this->start() ?: $this->getResponse();
        } catch (ResponseException $ex) {
            return $ex->getResponse();
        } catch (\Exception $ex) {
            if ($throwException) {
                $error = json_decode($ex->getMessage(), 1);
                if (is_array($error)) {
                    $error['status'] = 'error';
                } else {
                    $error = [
                    'status'=>'error', 
                    'message'=>$ex->getMessage(), 
                    'trace'=>explode("\n", $ex->getTraceAsString())
                    ];
                }
                return new Response(400, $error);
            }
            throw $ex;
        }
        
    }
    
    public function getFormatter($type) {
        $locale = Util\nvl($this->getUser()->get('locale'), 'it-IT');
        switch($type) {
            case 'money' :
                return new \NumberFormatter($locale, \NumberFormatter::CURRENCY);
                break;
            case 'date' :
                return new Formatter\DateFormatter($locale);
            case 'datetime' :
                return new Formatter\DateTimeFormatter($locale);
        }
        
        throw new Util\Error\NotFoundException();
    }

    public function normalizeQuery($query) {
        // ricerca del pattern @DataSource._@ per poter utilizzare risorse di database differenti
        if (preg_match_all("/\@DataSource\.([^@]*)\@/", $query, $matches)) {
            foreach($matches[0] as $k=>$v) {
                $nDS = $this->getDataSource($matches[1][$k]);
                $query = str_replace($v, $nDS->getAdapter()->getName(), $query);
            }
        }
        return $query;
    }
}
