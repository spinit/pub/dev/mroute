<?php

namespace Spinit\Dev\MRoute;

use Spinit\Util;

/**
 * Description of ControllerMap
 *
 * @author ermanno
 */
class XCommandMap {
    
    static private $maps;
    
    public static function get() {
        $args = func_get_args();
        $nameClass = array_shift($args);
        return Util\getInstanceArray(Util\arrayGet(self::$maps, $nameClass, $nameClass), $args);
    }
    
    public static function set($name, $cpath) {
        self::$maps[$name] = $cpath;
    }
}
