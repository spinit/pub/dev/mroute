<?php
namespace Spinit\Dev\MRoute;

use Spinit\Lib\DataSource;
use GuzzleHttp\Psr7\Request;

use Spinit\Util;
use Spinit\Dev\MRoute\Core\Type\MainInterface;
use Spinit\Dev\MRoute\Core\InstanceRegister;
use Webmozart\Assert\Assert;

use Spinit\Dev\MRoute\Instance;

/**
 * Description of Router
 *
 * @author ermanno.astolfi
 */

class Main implements MainInterface {
    
    use DataSource\GetDataSourceTrait;
    use Util\ParamTrait;
    
    private $home;
    
    public function __construct($DataSource, $home = '') {
        $this->setDataSource($DataSource);
        $this->home = dirname(getcwd()).'/'.Util\nvl($home, 'home');
        if (!is_dir($this->home)) {
            @mkdir($this->home, 0755);
        }
        if (!is_writable($this->home)) {
            debug('Directory '.$this->home.' non scrivibile. Abilitare i permessi necessari.');
        }
    }
    /**
     * Funzione configurabile per poter l'individuazione del manager da attivare
     * @param Request $request
     * @return type
     */
    public function getInstance($request)
    {
        // classe preposta alla generazione delle istanze
        Assert::notNull($request,'Request non impostata');
        try {
            // ricerca dalla istanza relativa alla richiesta pervenuta
            $cmd = Util\FactoryMap::get(getArch($this), 'Spinit:Dev:MRoute:Command:GetInstanceInfo', $this);
            $info = $cmd->exec($request);
            if ($info) {
                // trovata ... allora viene caricata
                $manager = $this->makeInstance($info);
                $manager->trigger('found');
            } else {
                // altrimenti viene fatto gestire dal NOT FOUND 
                $manager = new Instance\NotFoundInstance\Manager($this);
            }
        } catch (Util\Error\NotFoundException $e) {
            // se non è presente la struttura di gestione delle istanza ... parte l'installazione
            $manager = new Instance\InstallationInstance\Manager($this);
        }
        
        // viene impostata la richiesta da dover processare
        $manager->setRequest($request);
        // viene inviato l'evento di inzio lavoro
        $manager->trigger('init');
        return $manager;
    }
    
    /**
     * Generazione Gestore istanza partendo del record che lo descrive
     * @param type $info
     * @return type
     */
    public function makeInstance($info)
    {
        // se l'istanza è da installare ... viene caricato il manager preposto
        if (Util\arrayGet($info, ['stat', 'event']) == 'install') {
            return new Instance\InstallationInstance\Manager($this, $info);
        }
        Assert::notEmpty(Util\arrayGet($info, 'manager'), 'Manager non impostato');
        // creazione
        $instance = Util\getInstance(Util\arrayGet($info, 'manager'), $this, $info);
        // configurazione ... se impostato nel registro
        $config = InstanceRegister::getInstanceManager(Util\arrayGet($info, 'manager'));
        
        if ($init = Util\arrayGet($config, 'init')) {
            if (is_callable($init)) {
                \call_user_func($init, $instance, $this);
            } else if (is_array($init)) {
                foreach($init as $key => $value) {
                    $instance->setParam($key, $value);
                }
            }
        }
        return $instance;
    }
    
    public function decrypt($token) {
        $secret = Util\getenv('SECRET_KEY') ?: md5(__FILE__);
        list ($encrypt, $iv) = explode(':', $token);
        return @openssl_decrypt($encrypt, 'aes-256-cbc', $secret, 0, base64_decode($iv));
    }
    
    public function encrypt($data) {
        $secret = Util\getenv('SECRET_KEY') ?: md5(__FILE__);
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $encrypt = @openssl_encrypt ($data, 'aes-256-cbc', $secret, 0, $iv);
        return $encrypt.':'.base64_encode($iv);
        
    }
    public function getHome() {
        return $this->home;
    }
}
