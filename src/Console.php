<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute;

use Spinit\Util;
/**
 * Description of Console
 *
 * @author ermanno
 */
class Console {
    use Core\HasInstance;
    
    private $root;
    
    public function __construct($root) {
        date_default_timezone_set('Europe/Rome');
        $this->root = $root;
        if (!is_dir($this->root)) {
            mkdir($this->root, 0755, true);
        }
    }
    public function getRoot() {
        return $this->root;
    }
    public function reset() {
        $fname = fspath($this->root, 'console.log');
        $fp = fopen($fname, 'w');
        if ($fp) {
            fwrite($fp, '');
            fclose($fp);
        }
    }
    public function log() {
        $args = func_get_args();
        
        $e = new \Exception();
        $firma = '';
        $data = Util\arrayGet($e->getTrace(), 0);
        if ($data and key_exists('file', $data)) {
            $firma = '== '. $data['file'].':'.$data['line']." == ";
        }
        
        $head = date('Y-m-d H:i:s')." [".getmypid()."] ".$firma;
        $body = "";
        if (count($args) and is_string($args[0])) {
            $head .= array_shift($args);
        }
        while(count($args)) {
            $item = array_shift($args);
            $body .= (is_string($item)?$item:json_encode($item, JSON_PRETTY_PRINT))."\n";
        }
        $fname = fspath($this->root, 'console.log');
        // se il giorno dell'ultima modifica non è oggi ... viene azzerato
        $lastDate = date('Y-m-d', @filemtime($fname));
        if ($lastDate != date('Y-m-d')) {
            $fp = fopen($fname, 'w');
        } else {
            $fp = fopen($fname, 'a');
        }
        if ($fp) {
            fwrite($fp, $head."\n".$body);
            fclose($fp);
        }
    }
    
    public function getLog() {
        $fname = fspath($this->root, 'console.log');
        $file = file($fname);
        $file = array_reverse($file);
        return $fname ." ".date('H:i:s')." : \n\n".implode("", $file);
    }
}
