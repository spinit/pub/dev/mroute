<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Helper;

use Spinit\Util;
/**
 * Description of ImageViewer
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ImageViewer
{
    private $rootDir;
    
    public function __construct($root)
    {
        $this->rootDir = $root;
    }
    
    function resize_image($file, $w, $h, $crop=FALSE) {
        list($width, $height, $type) = getimagesize($file);
        $func = '';
        switch($type) {
            case IMAGETYPE_GIF :
                $func = 'imagecreatefromgif';
                break;
            case IMAGETYPE_PNG :
                $func = 'imagecreatefrompng';
                break;
            case IMAGETYPE_JPEG :
                $func = 'imagecreatefromjpeg';
                break;
        }
        if (!$func or (!$w and !$h)) {
            return false;
        }
        $r = $width / $height;
        if (!$w) {
            $w = $h * $r;
        }
        if (!$h) {
            $h = $w/$r;
        }
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width-($width*abs($r-$w/$h)));
            } else {
                $height = ceil($height-($height*abs($r-$w/$h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w/$h > $r) {
                $newwidth = $h*$r;
                $newheight = $h;
            } else {
                $newheight = $w/$r;
                $newwidth = $w;
            }
        }
        $src = $func($file);
        $dst = \imagecreatetruecolor($newwidth, $newheight);
        \imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        return $dst;
    }

    public function render($path)
    {
        $fname = '';
        if ($path) {
            $args = explode('/', trim($path, '/'));
            $farg = array_shift($args);
            $part = explode('.', $farg);
            $uRoot = $this->rootDir.'/object';
            $iRoot = $this->rootDir.'/info';
            $fname = '/'.substr($part[0],0,2).'/'.substr($part[0],2,2).'/'.$part[0];
        }
        if ($fname and is_file($uRoot.$fname)) {
            if (is_file($iRoot.$fname.'.json')) {
                $conf = json_decode(file_get_contents($iRoot.$fname.'.json'), 1);
            }
            if (isset($_GET['download'])) {
                if ($rname = Util\arrayGet($conf, 'name')) {
                    header('Content-Disposition: attachment; filename="'.$rname.'"');
                } else {
                    header('Content-Disposition: attachment; filename="file-scaricato"');
                }
            } else {
                if ($rname = Util\arrayGet($conf, 'name')) {
                    header('Content-Disposition: filename="'.$rname.'"');
                }
            }
            if ($resize = array_shift($args)) {
                $dim = explode('x', $resize);
                $dim[0] = Util\arrayGet($dim, 0);
                $dim[1] = Util\arrayGet($dim, 1);
                $image = $this->resize_image($uRoot.$fname, $dim[0], $dim[1]);
                \header('Content-Type: image/jpeg');
                \imagejpeg($image);

            } else {
                \header('Content-type: '.Util\arrayGet($conf, 'type'));
                \readfile($uRoot.$fname);
            }
        } else {
            self::notFound();
        }
        throw new Util\Error\StopException();
    }
    
    public static function notFound()
    {
        \header('Content-type: image/png');
        \readfile(__DIR__.'/ImageNotFound.png');
    }
}
