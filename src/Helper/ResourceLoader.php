<?php

namespace Spinit\Dev\MRoute\Helper;

use Spinit\Util;
use Spinit\Util\Error\NotFoundException;

use function Spinit\Util\arrayGet;

class ResourceLoader {
    private $rdir;
    private $types;
    private $cacheDir;

    public function __construct($rdir, $types = []) {
        $this->rdir = $rdir;
        Util\checkDir($rdir);
        $this->types = $types;
        $this->cacheDir = fspath(sys_get_temp_dir(), 'osy-img');
    }
    
    public function onlyImages() {
        $this->types = [IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG];
    }

    public function getList() {
        $odir = dir($this->rdir);
        while (false !== ($entry1 = $odir->read())) {
            if (in_array($entry1, ['.', '..'])) {
                continue;
            }
            $level1 = dir($odir->path.'/'.$entry1);
            while (false !== ($entry2 = $level1->read())) {
                if (in_array($entry2, ['.', '..'])) {
                    continue;
                }
                $level2 = dir($level1->path.'/'.$entry2);
                while (false !== ($fimage = $level2->read())) {
                    if (in_array($fimage, ['.', '..'])) {
                        continue;
                    }
                    if ($fimage[0] == '.') {
                        continue;
                    }
                    $fpath = $level2->path.'/'.$fimage;
                    if (count($this->types)) {
                        $info = getimagesize($fpath);
                        if (!in_array($info[2], $this->types)) {
                            continue;
                        }
                    }
                    yield $this->getRecord($fpath, $fimage);
                }
                $level2->close();
            }
            $level1->close();
        }
        $odir->close();        
    }
    public function setCacheDir($cacheDir) {
        $this->cacheDir = $cacheDir;
    }
    public function getResource($name) {
        $fpath = fspath($this->rdir, substr($name, 0, 2), substr($name, 2, 2), $name);
        if (!is_file($fpath)) {
            throw new NotFoundException('not found : '.$name);
        }
        $handle = fopen($fpath, "rb");
        $rec = null;
        if (!$handle) {
            throw new NotFoundException('error in open : '.$name);
        }
        $first = fgets($handle);
        $rec = json_decode($first, 1);

        $rec['write'] = function($file) use (&$handle, &$rec) {
            $closeFile = false;
            if (is_string($file)) {
                $fp = fopen($file, 'wb');
                $closeFile = true;
            } else {
                $fp = file;
            }
            if (!$fp) {
                return false;
            }
            while ($content = fread($handle, 1024 * 8)) {
                fwrite($fp, $content);
            }
            fclose($imgFile);
            if ($closeFile) {
                fclose($fp);
            }
            return true;
        };

        $rec['load'] = function ($w = '', $h = '') use (&$handle, &$rec) {
            if ((!$w and !$h) or substr($rec['type'], 0, 6) != 'image/') {
                while (!feof($handle)) {
                    echo fgets($handle, 8192);
                }
                fclose($imgFile);
                return;
            }
            $osyName = $w.'x'.$h;
            $dir2 = Util\checkDir(fspath($this->cacheDir, $osyName));
            $imgFile = fspath($dir2, $rec['cod']);
            if (is_file($imgFile)) {
                readfile($imgFile);
                return;
            }
            $dir1 = sys_get_temp_dir() .'/osy-tmp';
            if (!is_dir($dir1)) {
                mkdir($dir1);
            }
            $tmpfname = tempnam($dir1 , "IMG-");
            
            ob_start();
            while (!feof($handle)) {
                echo fgets($handle, 8192);
            }
            fclose($handle);
            $handle = fopen($tmpfname, "w");
            fwrite($handle, ob_get_clean());
            fclose($handle);

            ob_start();
            $this->resizeImage($tmpfname, $w, $h);
            unlink($tmpfname);
            file_put_contents($imgFile, ob_get_clean());
            readfile($imgFile);
        };
        $rec['resname'] = $name;
        return $rec;
    }

    private function resizeImage ($file_name, $width, $height, $crop=FALSE) {
        list($wid, $ht, $type) = getimagesize($file_name);
        switch($type) {
            case IMAGETYPE_JPEG:
                $create = 'imagecreatefromjpeg';
                $write = 'imagejpeg';
                break;
            case IMAGETYPE_PNG:
                $create = 'imagecreatefrompng';
                $write = 'imagepng';
                break;
            case IMAGETYPE_GIF:
                $create = 'imagecreatefromgif';
                $write = 'imagegif';
                break;
                    }
        $r = $wid / $ht;
        if (!$width and !$height) {
            $width = $wid;
            $height = $ht;
        }
        if (!$width) {
            $width = $wid * $height / $ht;
        }
        if (!$height) {
            $height = $ht * $width / $wid;
        }
        if ($crop) {
           if ($wid > $ht) {
              $wid = ceil($wid-($width*abs($r-$width/$height)));
           } else {
              $ht = ceil($ht-($ht*abs($r-$w/$h)));
           }
           $new_width = $width;
           $new_height = $height;
        } else {
           if ($width/$height > $r) {
              $new_width = $height*$r;
              $new_height = $height;
           } else {
              $new_height = $width/$r;
              $new_width = $width;
           }
        }
        $source = call_user_func($create, $file_name);
        $dst = imagecreatetruecolor($new_width, $new_height);
        imagecopyresampled($dst, $source, 0, 0, 0, 0, $new_width, $new_height, $wid, $ht);
        call_user_func($write, $dst);
        imagedestroy($dst);
        return;
    }    
    
    private function getRecord($fpath, $fimage) {
        $handle = fopen($fpath, "r");
        $rec = null;
        if ($handle) {
            $first = fgets($handle);
            fclose($handle);
            $rec = json_decode($first, 1);
            $rec['resname'] = $fimage;
        }         
        return $rec;
    }
}
