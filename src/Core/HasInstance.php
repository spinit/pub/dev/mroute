<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core;

use Spinit\Dev\MRoute\Core\Type\InstanceInterface;

/**
 * Description of HasInstance
 *
 * @author ermanno
 */
trait HasInstance {
    private $instance;
    
    protected function setInstance(InstanceInterface $instance) {
        $this->instance = $instance;
        return $this;
    }
    public function getInstance() {
        return $this->instance;
    }
    public function getDataSource($name='') {
        return $this->instance->getDataSource($name);
    }            
}
