<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core\Exception;

use Spinit\Dev\MRoute\Core\Response;

/**
 * Description of ResponseException
 *
 * @author ermanno
 */
class ResponseException extends \Exception {
    public function __construct($response) {
        parent::__construct();
        if (!($response instanceof Response)) {
            $response = new Response($response);
        }
        $this->response = $response;
    }
    
    public function getResponse() {
        return $this->response;
    }
}
