<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core;

use Spinit\Dev\MRoute\Core\HasInstance;
use Spinit\Dev\MRoute\Core\Type\InstanceInterface;
use Spinit\Util;
use Spinit\Util\Error\NotFoundException;

/**
 * Description of Router
 *
 * @author ermanno
 */

class Router {
    
    use HasInstance;
    
    private $tracks = [];
    
    public function __construct(InstanceInterface $instance) {
        $this->setInstance($instance);
    }
    
    public function set($track, $runner, $conf = null) {
        $this->tracks[$track] = ['name' => $runner, 'conf' => $conf];
    }
    
    public function run ($url, $default = null, $conf = null) {
        
        $found = false;
        $url = rtrim($url, '/').'/';
        foreach($this->tracks as $path => $runner) {
            $path = ltrim(rtrim($path, '/').'/', '/');
            if ($path and substr($url, 0, strlen($path)) != $path ) {
                continue;
            }
            if (!$found or strlen($found['path']) < strlen($path)) {
                $found = ['path'=>$path, 'runner' => $runner];
            }
        }
        if (!$found and $default) {
            $found = ['path'=>'', 'runner' => ['name'=>$default, 'conf'=>$conf]];
        }
        if (!$found) {
            throw new NotFoundException('Runner not found for '.$url);
        }
        $suburl = trim(substr($url, strlen($found['path'])), '/');
        if (is_callable($found['runner']['name'])) {
            return call_user_func($found['runner']['name'], $suburl, $found['runner']['conf']);
        }
        $runner = Util\getInstanceArray($found['runner']['name'], $this->getInstance(), $found['runner']['conf']?:[]);
        return $runner->run($suburl);
    }
}
