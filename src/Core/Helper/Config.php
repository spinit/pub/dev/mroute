<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core\Helper;

use Spinit\Util;

/**
 * Description of BookingConfig
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Config
{
    private $instance;
    private $conf;
    private $data;
    
    public function __construct($instance)
    {
        $this->instance = $instance;
        $this->conf = loadConfig($this);
        $this->data = [];
        $list = $this->instance->getDataSource()->query($this->conf->get('config'), ['user' => $this->instance->getUser('id')]);
        foreach($list as $rec) {
            $this->data[$rec['urn']] = $rec['val'];
        }
    }
    
    public function get($name, $default = '')
    {
        return Util\arrayGet($this->data, explode('.', $name), $default);
    }
}
