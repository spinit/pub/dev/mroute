<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core;

use Spinit\Util;
use Spinit\Dev\MRoute\Core\Type\InstanceInterface;
use Spinit\Util\ParamTrait;

/**
 * Description of Track
 *
 * @author ermanno
 */
abstract class Track {
    
    use HasInstance;
    use ParamTrait;
    
    private $root;
    private $query;
    private $body;
    private $eparam;
    
    public function __construct(InstanceInterface $instance, $root, $prefix = '') {
        $this->setInstance($instance);
        $this->root = rtrim($root, '/');
        $this->prefix = $prefix;
        parse_str($this->getRequest()->getUri()->getQuery(), $this->query);
        $ctype = explode(';', $this->getRequest()->getHeaderLine('Content-Type'));
        switch($ctype[0]) {
            case 'application/json':
                $this->body = json_decode($this->getRequest()->getBody(), 1);
                break;
            case 'multipart/form-data':
                $this->body = $_POST;
                break;
            default:
                parse_str($this->getRequest()->getBody(), $this->body);
                break;
        }
    }
    
    abstract public function run ($path);
    
    public function setEventParam($param) {
        $args = func_get_args();
        $name = array_shift($args);
        if (count($args) < 1) {
            if (is_string($param)) {
                $test = json_decode($param, 1);
                $param = is_array($test) ? $test : $param;
            }
            $this->eparam = $param;
        } else {
            $this->eparam[$name] = array_shift($args);
        }
        return $this;
    }
    public function getEventParam() {
        $args = func_get_args();
        
        if (count($args) < 1) {
            return $this->eparam;
        }
        return Util\arrayGet($this->eparam, array_shift($args));
    }
    public function getRequest() {
        return $this->getInstance()->getRequest();
    }
    
    public function getResponse() {
        return $this->getInstance()->getResponse();
    }
    
    public function getUrl($resourceName) {
        $filename = fspath($this->root, 'html', ltrim($resourceName, '/'));
        $base = $this->getInstance()->getInfo('url') ?: $this->getInstance()->getPath();
        $base = rtrim($base, '/').'/';
        $url = '//'.rtrim($base.$this->prefix.'/','/') . '/'. ltrim($resourceName, '/');
        if (is_file($filename)) {
            $url .= '?v=' . sha1($filename);
        }
        return  $url;
    }

    public function getQuery() {
        $args = func_get_args();
        if (!count($args)) {
            return $this->query;
        }
        array_unshift($args, $this->query);
        return call_user_func_array("Spinit\\Util\\arrayGet", $args);
    }
    public function getBody() {
        $args = func_get_args();
        if (!count($args)) {
            return $this->body;
        }
        array_unshift($args, $this->body);
        return call_user_func_array("Spinit\\Util\\arrayGet", $args);
    }
    
    public function getRoot()
    {
        return $this->root;
    }
    public static function getMimeType($idx) {

        $mimet = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',
            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',
            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',
            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',
            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        if (isset($mimet[$idx])) {
            return $mimet[$idx];
        } else {
            return '';
        }
    }

    
}
