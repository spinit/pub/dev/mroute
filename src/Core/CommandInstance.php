<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core;

use Spinit\Dev\MRoute\Core\Command;
use Spinit\Dev\MRoute\Core\Type\InstanceInterface;

/**
 * Description of CommandInstance
 *
 * @author ermanno
 */
abstract class CommandInstance extends Command {
    public function __construct(InstanceInterface $instance) {
        parent::__construct($instance->getMain(), $instance);
    }
    public function getResponse() {
        return $this->getInstance()->getResponse();
    }
    public function getRequest() {
        return $this->getInstance()->getRequest();
    }
}
