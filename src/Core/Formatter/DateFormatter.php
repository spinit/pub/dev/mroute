<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core\Formatter;

use Spinit\Dev\MRoute\Core\Formatter;
/**
 * Description of DateFormatter
 *
 * @author ermanno
 */
class DateFormatter extends Formatter {
    
    public function format($str) {
        $type = '';
        if (func_num_args()>1) {
            $type = func_get_arg(1);
        }
        if (!$type) {
            $type = '/';
        }
        $part = explode(' ', $str);
        $tok = explode($type == '-' ? '/' : '-', $part[0]);
        if (count($tok)>1) {
            $tok = array_reverse($tok);
        }
        return implode($type == '/' ? '/' : '-', $tok);
    }
}
