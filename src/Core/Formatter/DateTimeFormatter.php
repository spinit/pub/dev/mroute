<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core\Formatter;

/**
 * Description of DateFormatter
 *
 * @author ermanno
 */
class DateTimeFormatter extends DateFormatter {
    
    public function format($str) {
        $type = '';
        if (func_num_args()>1) {
            $type = func_get_arg(1);
        }
        $part = explode(' ', $str);
        $part[0] = parent::format($part[0], $type);
        if (count($part)>1) {
            $tok = explode(':', $part[1]);
            $part[1] = array_shift($tok).':'.array_shift($tok);
        }
        return implode(' ', $part);
    }
}
