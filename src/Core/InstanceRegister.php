<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core;

use Spinit\Util;

/**
 * Description of InstanceRegister
 *
 * @author ermanno
 */

class InstanceRegister {
    
    private static $managerType = [];
    
    static function setInstanceManager($classPath, $name, $require, $requireAdmin, $init = null) {
        self::$managerType[Util\getColonsPath($classPath)] = [
            'name'=>$name,
            'require'=>$require,
            'requireAdmin'=>$requireAdmin,
            'init' => $init
        ];
    }
    static function getInstanceManager($classPath) {
        return Util\arrayGet(self::$managerType, Util\getColonsPath($classPath), []);
    }
    static function getInstanceManagerList() {
        return self::$managerType;
    }
}
