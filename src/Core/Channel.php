<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core;

use Spinit\Util;

/**
 * Description of Channel
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Channel
{
    private $list = [];
//    private $wsList = [];
    private $lastTime = 0;
    use Util\ParamTrait;
    
    public function __construct($wsList = [])
    {
        date_default_timezone_set("Europe/Rome");
        header('Content-Type: text/event-stream; charset=utf-8');
        header('Cache-Control: no-cache');
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Methods: GET');
        header('Access-Control-Expose-Headers: X-Events');
        header("Access-Control-Allow-Origin: *");
        @ini_set('zlib.output_compression',0);

        @ini_set('implicit_flush',1);

        @ob_end_clean();

        set_time_limit(0);
        
        ignore_user_abort(true);
        ob_implicit_flush();
    
//        $this->wsList = $wsList;
/*
        $this->loop = React\EventLoop\Factory::create();
        $this->connector = new Ratchet\Client\Connector($this->loop);
        
        $app = function (Ratchet\Client\WebSocket $conn) use (&$app) {
            $conn->on('message', function (\Ratchet\RFC6455\Messaging\MessageInterface $msg) use ($conn) {
                echo "Received: {$msg}\n";
                $conn->close();
            });

            $conn->on('close', function ($code = null, $reason = null) use ($app) {
                echo "Connection closed ({$code} - {$reason})\n";

                //in 3 seconds the app will reconnect
                $this->loop->addTimer(3, function () use ($app) {
                    $this->connectToServer('endPoint', $app);
                });
            });

            $conn->send('Hello World!');
        };

        if (count($this->wsList)) {
            foreach($this->wsList as $endPoint) {
                $this->connectToServer($endPoint, $app);
            }
        }
    }
    
    private function connectToServer ($endPoint, $app)
    {
        $this->connector($endPoint, ['protocol1', 'subprotocol2'], ['Origin' => 'http://localhost'])
            ->then($app, function (\Exception $e){
                echo "Could not connect: {$e->getMessage()}\n";
                $this->loop->stop();
            });
 * 
 */
    }
    public function ping($maxtime = 10, $message = '', $event = 'ping')
    {
        if ($this->lastTime < time() - $maxtime) {
            $this->send($message ?: date('Y-m-d H:i:s'), $event);
        }
        return $this;
    }
    public function send($message, $event = '')
    {
        $this->checkIsAlive();
        $this->stampa($message, $event);
        return $this;
    }
    public function exec()
    {
        $args = func_get_args();
        array_unshift($args, 'command');
        // viene inviata l'esecuzione di una lista di comandi ... con solo un comando
        $this->send(['exec'=>[$args]]);
        return $this;
    }
    private function stampa($message, $event = '')
    {
        /*
        if (is_array($message) and !isset($message['time'])) {
            $message['time'] = [time(),date('d/m/Y H:i:s')];
        }
         * 
         */
        $this->lastTime = time();
        echo "event: ".($event?:'message')."\r\n";
        if (!is_string($message)) {
            $message = json_encode($message);
        }
        // invio messaggio
        foreach(explode("\n", $message) as $line) {
            $this->writeData(trim($line));
        }
        // end message
        $this->writeData("\r\n");
        $this->flushData();
        return $this;
    }
    private function writeData($data)
    {
        echo "data: ".$data."\r\n";
    }
    private function flushData()
    {
        // riempimento forzato di un eventuale buffer
        for ($i = 0; $i < 8; $i++) {
            echo str_repeat(' ', 1024)."\r\n";
        }
        while(ob_get_level()>1) {
            ob_end_flush();
        }
        flush();
    }
    public function add($runner)
    {
        $this->list[] = $runner;
        return $this;
    }
    public function run($timeSleep = 1)
    {
        $start = 0;
        while(true) {
            foreach($this->list as $runner) {
                $this->checkIsAlive();
                call_user_func($runner, function ($message, $event = ''){
                    $this->stampa($message, $event);
                }, $this);
            }
            while (time() - $start < $timeSleep) {
                usleep(500 * 1000); // 1/2 di sec
                $this->checkIsAlive();
            }
            $start = time();
        }
        throw new StopException();
    }
    public function each($list)
    {
        foreach($list as $message) {
            $this->checkIsAlive();
            $this->stampa($message);
        }
        throw new StopException();
    }
    
    public function checkIsAlive()
    {
        if(connection_status()!=0){
            throw new StopException();
        }
        if(connection_aborted()){
            throw new StopException();
        }
    }
}
