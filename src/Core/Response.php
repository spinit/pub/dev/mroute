<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core;

use Spinit\Util;
use Spinit\Dev\MRoute\Core\Exception\ResponseException;
use Spinit\Dev\MRoute\Core\Exception\StopException;
use Spinit\Dev\MRoute\Console;

use function Spinit\Util\arrayGet;

/**
 * Description of Response
 *
 * @author ermanno
 */

class Response extends Util\DictionaryBase {
    
    private $statusCode = 200;
    private $content = null;
    private $headers = [];
    private $writer;
    private $thens = [];
   
    
    public function __construct() {
        $args = func_get_args();
        $writer = null;
        $data = [];
        foreach($args as $arg) {
            if (is_integer($arg)) {
                $this->setStatusCode($arg);
            } else if(is_array($arg)) {
                $data = $arg;
            } else if (is_callable($arg)) {
                $writer = $arg;
            }
        }
        if (!array_key_exists('status', $data)) $data['status'] = 'success';
        parent::__construct($data);
        
        $this->writer = $writer;
        if (!$writer) {
            // header di default
            $this->setHeader('Content-Type', 'application/json');
        }
        
        // invio contenuto dati e chiusura connessione
        $this->then(function($stream) {
            try {
                $content = $this->toString();
                if (!$stream) {
                    http_response_code($this->getStatusCode());
                    $this->writeHeaders();
                    // get the size of the output
                    echo $content;

                } else {
                    $stream->write($content);
                }
            } catch (ResponseException $resp) {
                $resp->getResponse()->send($stream);
            } catch (\Exception $e) {
                debug($e->getMessage(), explode("\n", $e->getTraceAsString()));
            }
        });
    }
    public function toString() {
        ob_start();
        $output = call_user_func($this->writer ?: [$this, 'defaultWriter'], $this);
        $content = ob_get_clean(). $output;
        return $content;
    }
    public function __toString() {
        ob_start();
        $this->send();
        $content = ob_get_clean();
        return $content;
    }
    // funzioni da richiamare in cascata in fase di invio (send) della richiesta
    public function then($call, $prepend = 0) {
        if ($prepend) {
            array_unshift($this->thens, $call);
        } else {
            $this->thens[] = $call;
        }
        return $this;
    }
    public function addHeader($name, $value) {
        $this->headers[$name][] = $value;
        return $this;
    }
    
    public function getHeaders() {
        return $this->headers;
    }
    public function setHeader($name, $value) {
        $this->headers[$name] = [$value];
        return $this;
    }
    public function getHeader($name) {
        return arrayGet($this->getHeaderList($name), 0);
    }
    public function getHeaderList($name) {
        return arrayGet($this->headers, $name);
    }
    protected function writeHeaders() {
        if (strpos($_SERVER['argv'][0], 'phpunit') !== false) {
            return;
        }
        foreach($this->headers as $name => $list) {
            foreach($list as $value) {
                header("{$name}: {$value}");
            }
        } 
    }
    
    public function send($stream = null) {
        $console = new Console('/tmp');
        try {
            foreach($this->thens as $k=>$then) {
                if (count($this->thens) > 1) {
                    $console->log('send '.count($this->thens).' k = '.$k);
                }
                $ret = call_user_func($then, $stream); //$then, $this, $console);
            }
        } catch (StopException $e) {
        } catch (\Exception $e) {
            $console->log($e->getMessage(), explode("\n", $e->getTraceAsString()));
        }
        return $this;
    }
    public function setContent($content, $contentType = '') {
        if ($contentType) {
            $this->setHeader('Content-Type', $contentType);
        }
        $this->set('page.content', $content);
        $this->throwException();
    }
    
    public function setStatusCode($status) {
        $this->statusCode = $status;
        return $this;
    }
    
    public function getStatusCode()
    {
        return $this->statusCode;
    }
    
    protected function defaultWriter()
    {
        $content = $this->get('page.content');
        if ($content === null) {
            $content = \json_encode($this->asArray(), JSON_PRETTY_PRINT);
        } else if (is_callable($content)){
            ob_start();
            $ret = call_user_func($content, $this);
            $content = $ret.ob_get_clean();
        }
        return $content;
    }
    public function throwException()
    {
        throw new ResponseException($this);
    }
    
    public function addData($name, $value)
    {
        $this->append("data.{$name}", $value);
        return $this;
    }
    
    public function addExec()
    {
        $this->append('data.exec', func_get_args());
        return $this;
    }
    public function addCommand()
    {
        $args = func_get_args();
        array_unshift($args, 'command');
        return call_user_func_array([$this, 'addExec'], $args);
    }
    public function addTrigger()
    {
        $args = func_get_args();
        array_unshift($args, 'trigger');
        return call_user_func_array([$this, 'addExec'], $args);
    }
    public function addContent()
    {
        $args = func_get_args();
        array_unshift($args, 'content');
        return call_user_func_array([$this, 'addExec'], $args);
    }
    /*
    public function addModal()
    {
        $args = func_get_args();
        array_unshift($args, 'modal');
        return call_user_func_array([$this, 'addExec'], $args);
    }
    */
    public function addBox()
    {
        $args = func_get_args();
        array_unshift($args, 'box');
        return call_user_func_array([$this, 'addExec'], $args);
    }
    public function addError()
    {
        return $this->addData('error', func_get_args());
    }
    public function hasError()
    {
        return count($this->get('data.error')?:[]);
    }
    public function modal()
    {
        $args = func_get_args();
        array_unshift($args, 'modal');
        return $this->addData('exec', $args);
    }
}

