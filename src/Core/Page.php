<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core;

use GuzzleHttp\Psr7\Request;
use Spinit\Util;

use Spinit\Dev\MRoute\Core\HasInstance;
use Spinit\Dev\MRoute\Core\Track;
use Spinit\Dev\MRoute\Type\InstanceInterface;
use Webmozart\Assert\Assert;
use Spinit\Util\Error\NotFoundException;
/**
 * Description of Page
 *
 * @author ermanno
 */

class Page extends Track {

    private $path = '';
    public function setPath($path) {
        list($this->path, ) = explode('?', implode('/', Util\asArray($path,'/')));
        $this->initPath();
    }
    
    public function run($path, $commandGet = false) {
        $this->setPath($path);
        $this->setEventParam($this->getBody(['_', 'osy', 'param']));
        $command = $this->getBody(['_','osy', 'event']);
        if (!$command and $commandGet) {
            $command = $this->getQuery(['_','osy', 'event']);
            $this->setEventParam($this->getQuery(['_', 'osy', 'param']));
        }
        if ($command) {
            $response = $this->execCommand($command);
        } else {
            $response = $this->getContent();
        }
        
        return $response ?: $this->getResponse();
    }
    protected function initPath() {
        $filename = fspath($this->getRoot(), 'html', $this->path, 'init.php');
        if (is_file($filename)) {
            include($filename);
        }
        return $this;
    }
    protected function check($path)
    {
        $args = [];
        $root = fspath($this->getRoot(), 'html');
        $filename = fspath($root, $path);
        if (is_file($filename)) {
            $info = pathinfo($filename);
            if(Util\arrayGet($info, 'extension') != 'php') {
                (new Response(function() use ($filename, $info) {
                    readfile($filename);
                    return '';
                }))
                ->setHeader('Content-Type', self::getMimeType(Util\arrayGet($info, 'extension')))
                ->throwException();
            }
        }
        while($path) {
            $filename = fspath($root, $path);
            if (is_file($filename)) {
                return [$filename, $args];
            }
            $filename = fspath($root, $path, 'index.php');
            if (is_file($filename)) {
                return [$filename, $args];
            }
            $filename = fspath($root, $path.'.php');
            if (is_file($filename)) {
                return [$filename, $args];
            }
            array_unshift($args, basename($path));
            $path = trim(dirname($path), '.');
        }
        $filename = fspath($root, 'index.php');
        if (is_file($filename)) {
            return [$filename, $args];
        }
        throw new NotFoundException("File non trovato : ".$filename);
    }
    
    protected function getContent() {
        list($filename, $args) = $this->check($this->path);
        $this->getResponse()->setHeader('Content-Type', 'text/html');
        try {
            ob_start();
            $return = require($filename);
            $content = ob_get_clean();
        } catch (\Exception $e) {
            echo ob_get_clean();
            throw $e;
        }
        if ($return instanceof Response) {
            $response = $return;
        } else {
            $response = $this->getResponse();
            if (is_array($return)) {
                $response->add($return);
            } elseif (is_string($return)) {
                $content .= $return;
            }
        }
        $response->set('page.content', $content);
        return $response;
    }

    
    protected function execCommand($command) {
        
        $filename = fspath($this->getRoot(), 'html', $this->path, "evn-{$command}.php");
        if (!is_file($filename)) {
            throw new NotFoundException('Comando non implementato : '.$command.' : '.$filename);
        }
        try {
            ob_start();
            $return = require($filename);
            $content = ob_get_clean();
        } catch (\Exception $e) {
            echo ob_get_clean();
            throw $e;
        }
        if ($return instanceof Response) {
            $response = $return;
        } else {
            $response = $this->getResponse();
            if (is_array($return)) {
                $response->add($return);
            } elseif (is_string($return)) {
                $content .= $return;
            }
        }
        if ($content) {
            $response['content'] = $content;
        }
        return $response;
    }

}
