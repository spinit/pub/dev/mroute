<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core;

use Spinit\Dev\MRoute\Core\Type\MainInterface;
use Spinit\Dev\MRoute\Core\Type\InstanceInterface;
use Spinit\Dev\MRoute\Core\Type\CommandInterface;

/**
 * Description of Command
 *
 * @author ermanno
 */
abstract class Command implements CommandInterface {
    
    private $main;
    private $instance;
    
    public function __construct(MainInterface $main = null, InstanceInterface $instance = null) {
        $this->main = $main;
        if ($instance) {
            $this->setInstance($instance);
        }
    }
    
    /**
     * 
     * @return MainInterface
     */
    public function getMain() {
        return $this->main;
    }
    
    /**
     * 
     * @return InstanceInterface
     */
    public function getInstance() {
        return $this->instance;
    }
    public function setInstance(InstanceInterface $instance) {
        $this->instance = $instance;
        $this->main = $instance->getMain();
    }
    public function getDataSource($name = '') {
        if ($this->instance) {
            return $this->getInstance()->getDataSource($name);
        }
        return $this->getMain()->getDataSource();
    }
}
