<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core;

/**
 * Description of DateFormatter
 *
 * @author ermanno
 */
abstract class Formatter {
    private $locate;
    public function __construct($locate) {
        $this->locate = $locate;
    }
    
    public function getLocate() {
        return $this->locate;
    }
    
    abstract public function format($arg);
}
