<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core\Type;

/**
 *
 * @author ermanno
 */
interface MainInterface {
    public function getInstance($request);
    public function getDataSource();
}
