<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Core;

use Spinit\Dev\MRoute\Core\Track;
use Spinit\Util\Error\NotFoundException;
use Webmozart\Assert\Assert;

/**
 * Description of Assets
 *
 * @author ermanno
 */
class Assets  extends Track {
    private $executor = null;
    private $args = [];

    public function setExecutor($executor, $param = []) {
        $this->executor = $executor;
        $this->setParam($param);
    }
    public function getArgPath() {
        return implode(DIRECTORY_SEPARATOR, $this->args);
    }
    public function run($path, $event = '', $data = []) {
        if (is_array($path)) $path = implode('/', $path);
        list($name, ) = explode('?', $path);
        $this->args = [];
        $info = pathinfo($name);
        list($filename, $ext) = $this->searchFile($name, $event);
        // se l'estensione "figurativa" è conosciuta ... allora viene impostato il relativo content-type
        if ($info['extension']!= 'php') {
            $ctype = self::getMimeType($info['extension']);
            if ($ctype) {
                $this->getResponse()->setHeader('Content-Type', $ctype);
            }
        }
        if ($ext == 'php') {
            ob_start();
            $this->setParam('asset', $this);
            if ($this->executor) {
                $this->executor->execCode(\file_get_contents($filename), $this->getParamList());
            } else {
                require($filename);
            }
            return ob_get_clean();
        } else {
            return \file_get_contents($filename);
        }
    }
    private function searchFile($name, $event = '') {
        // vengono cercati solo file NON EVENTO
        $ename = 'index';
        if (substr($name, 0, 4) == 'evn-') {
            $name = substr($name, 4);
        }
        if ($event) {
            // se invece è impostato un evento ... allora vengono cercati solo file EVENTO
            $ename = 'evn-'.$event;
        }
        $filename = fspath($this->getRoot(), $name);
        if (is_file($filename)) return [$filename, ''];

        $part = explode('/', $name);
        while(count($part)) {
            $filename = fspath($this->getRoot(), implode('/', $part).'/'.$ename.'.php');
            if (is_file($filename)) return [$filename, 'php'];
            // se non è un evento ... allora il controller potreppe essere un file con il suo stesso nome
            if (!$event) {
                $filename = fspath($this->getRoot(), implode('/', $part).'.php');
                if (is_file($filename)) return [$filename, 'php'];
            }
            // scendi di un livello
            array_unshift($this->args, array_pop($part));
        }
        throw new NotFoundException($name. ' ['.$filename.']');
    }
    public function getList() {
        $list = [];
        $depth = function($middle) use (&$depth, &$list) {
            $root = fspath($this->getRoot(), $middle);
            if (is_dir($root)) foreach(scandir($root) as $file) {
                if (in_array($file, ['.', '..'])) continue;
                $path = fspath($root, $file);
                $name = fspath($middle, $file);
                if (is_file($path)) {
                    $info = pathinfo($path);
                    $list[] = [
                        'name'=>$name, 
                        'type'=>$info['extension'], 
                        'hash'=>sha1_file($path)
                    ];
                } else {
                    $depth($name);
                }
            }
            return $list;
        };
        return $depth('');
    }

}
