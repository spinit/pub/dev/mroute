<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Command;

use Spinit\Dev\MRoute\Core\Command;
use Spinit\Util;
use Webmozart\Assert\Assert;
/**
 * Description of GetInstanceInfo
 *
 * @author ermanno
 */
class GetInstanceInfo extends Command {
    
    public function exec($request = null) {
        
        Assert::notNull($request,'Request non impostata');
        
        if (!$this->getDataSource()->check('osx_ice') or !$this->getDataSource()->check('osx_ice_url')) {
            throw new Util\Error\NotFoundException();
        }
        list($schema, $uri) = explode('://', $request->getUri());
        $conf = loadConfig($this);
        $rs = $this->getDataSource()->query($conf->get('instance'), ['url'=>rtrim($uri, '/').'/']);
        //debug($rs);
        $rec = $rs->first();
        if ($rec) {
            $rec['stat'] = json_decode($rec['stat'], 1);
            $rec['conf'] = json_decode($rec['conf'], 1);
            $rec['con_str'] = Util\arrayGet($rec['conf'], 'con_str', $rec['con_str']);
        }
        //debug($rec);
        return $rec;
        
    }
}
