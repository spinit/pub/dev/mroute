<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\MRoute\Command;

use Spinit\Dev\MRoute\Core\CommandInstance;
use Spinit\Dev\MRoute\Instance;
use Spinit\Util;
use Spinit\UUIDO;
use Spinit\Dev\MRoute\Core\Type\MainInterface;
use Spinit\Lib\DataSource\PDO\AdapterPDO;
/**
 * Description of CreateInstance
 *
 * @author ermanno
 */
class CreateInstance extends CommandInstance {
    
    public function exec($param = null) {
        list($conStr, $accPoints, $rec) = func_get_args();
        
        $DS = $this->getMain()->getDataSource();
        // verifica di accesso al database impostato
        $DSice = $this->getMain()->makeDataSource($conStr.':1');
        // .. connessione riuscita
        $m_ice = $this->getInstance()->getModel('Item');
        $m_ice->set($rec)
              ->set('con_str', $conStr)
              ->set('stat',['event'=>'install']);
        $m_ice->save();
        // viene impostato il contatore del datasource con la radice della istanza appena creata
        $DS->setCounter(new UUIDO\Maker($m_ice->get('id')));
        // impostazioni di accesso all'istanza
        $m_url = $this->getInstance()->getModel('ItemUrl');
        foreach(Util\asArray($accPoints) as $apConf) {
            if ($url = Util\arrayGet($apConf, 'url')) {
                $m_url->clear();
                $m_url->set(['url'=>$url, 'manager'=>Util\arrayGet($apConf, 'class'), 'id_ice'=>$m_ice->get('id'), 'act'=>'1']);
                $m_url->save();
            }
        }
        return $rec;
    }

}
