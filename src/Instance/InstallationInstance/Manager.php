<?php


namespace Spinit\Dev\MRoute\Instance\InstallationInstance;

use Spinit\Dev\MRoute\Instance;
use GuzzleHttp\Psr7\Request;
use Spinit\Dev\MRoute\Core\Response;
use Spinit\Dev\MRoute\Core\Page;
use Spinit\Util;
use Webmozart\Assert\Assert;
use Spinit\Lib\Model\Model;
use Spinit\Lib\Model\Adapter\Xml\ModelAdapterXml;

use GuzzleHttp\Psr7\Uri;

/**
 * Description of Manager
 *
 * @author ermanno
 */
class Manager extends Instance {
    const ROOT_DIR = __DIR__.'/../../../theme/install/';
    
    protected function start() {
        $root = $this->getMain()->getParam(Util\getColonsPath(self::class).'@theme:root', self::ROOT_DIR);
        $rootDir = rtrim(dirname(Util\arrayGet($this->getRequest()->getServerParams(), 'SCRIPT_NAME')), '/');
        $path = $this->getRequest()->getUri()->getPath();
        return (new Page($this, $root))->run(substr($path, strlen($rootDir)), true);
    }
    
    public function install($user) {
        $args = func_get_args();
        array_shift($args);
        $conString = array_shift($args);
        $accPoints = array_shift($args);
        Assert::notEmpty(Util\arrayGet($user, 'name'), "Utente non impostato");
        Assert::notEmpty(Util\arrayGet($user, 'passwd'), "Password non impostata");
        Assert::eq(Util\arrayGet($user, 'passwd2'), Util\arrayGet($user, 'passwd'), json_encode(['code'=>'passwd-miss-match', 'message'=>"Password diverse"]));
        
        if (Util\getenv('DEPLOY') == 'DEBUG') {
            // istanza per l'avvio dell'istallazione tramite docker
            array_unshift($accPoints, ['url'=>'localhost', 'class'=>'Spinit:Dev:MRoute:Instance:DebugInstance:Manager']);
        }
        
        $this->initModels();
        
        unset($user['passwd2']);
        $user['passwd'] = md5($user['passwd']);
        // creatore Istanza
        $cmd = $this->getCommand(':Spinit:Dev:MRoute:Command:CreateInstance', $this);
        
        // ora il manager Installer "dirige" l'stanza appena creata
        $this->info = $cmd->exec($conString, $accPoints, ['admin'=>$user, 'home'=>'master', 'name'=>'master']);
        $scheme = $this->getRequest()->getUri()->getScheme();
        //$cmd = "nohup curl -X POST -d \"_[osy][event]=go\" http://localhost > /dev/null 2> /dev/null &";
        if (is_array($accPoints) and count($accPoints)) {
            $cmd = "nohup curl -X POST -d \"_[osy][event]=go\" {$scheme}://{$accPoints[0]['url']} > /dev/null 2>/dev/null &";
            $this->getConsole()->log("Avvio installazione", $cmd);
            // avvia l'installazione aprendo la prima url disponibile
            $log = shell_exec($cmd);
        }
    }
    
    private function initModels()
    {
        $root = __DIR__.'/Model';
        foreach(scandir($root) as $name) {
            $fname = fspath($root, $name);
            if (is_file($fname)) {
                (new Model(new ModelAdapterXml($name, $fname), $this->getDataSource()))->init();
            }
        }
    }
    
    public function getModel($name) {
        $fname = fspath(__DIR__, 'Model', $name.'.xml');
        return new Model(new ModelAdapterXml($name, $fname), $this->getDataSource());
    }
}
