<?php

namespace Spinit\Dev\MRoute\Instance;

use Spinit\Dev\MRoute\Instance;
use GuzzleHttp\Psr7\Request;
use Spinit\Dev\MRoute\Core\Response;
use Spinit\Dev\MRoute\Core\Page;
use Spinit\Util;
use Spinit\Dev\MRoute\CommandMap;
use Webmozart\Assert\Assert;

/**
 * Description of EmptyManager
 *
 * @author ermanno
 */
class InstallationInstance extends Instance {
    
    const ROOT_DIR = __DIR__.'/../../theme/install/';
    
    public function start() {
        $root = $this->getMain()->getParam(Util\getColonsPath(self::class).'@theme:root', self::ROOT_DIR);
        return (new Page($this, $root))->run($this->getRequest()->getUri()->getPath());
    }
    
    public function createInstance($user, $conString, $accPoints) {
        
        Assert::notEmpty(Util\arrayGet($user, 'name'), "Utente non impostato");
        Assert::notEmpty(Util\arrayGet($user, 'passwd'), "Password non impostata");
        Assert::eq(Util\arrayGet($user, 'passwd2'), Util\arrayGet($user, 'passwd'), "Password diverse");
        unset($user['passwd2']);
        $user['passwd'] = md5($user['passwd']);
        // creazione Istanza
        $cmd = $this->getCommand(':Spinit:Dev:MRoute:Command:CreateInstance', $this->getMain());
        $cmd->exec($conString, $accPoints, ['admin'=>$user, 'home'=>'master', 'name'=>'master']);
    }
}
