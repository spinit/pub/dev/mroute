<?php

namespace Spinit\Dev\MRoute\Instance\NotFoundInstance;

use Spinit\Dev\MRoute\Instance;
use GuzzleHttp\Psr7\Request;
use Spinit\Dev\MRoute\Core\Response;
use Spinit\Dev\MRoute\Core\Page;
use Spinit\Util;
use Spinit\Dev\MRoute\Core\Exception\ResponseException;
/**
 * Description of EmptyManager
 *
 * @author ermanno
 */
class Manager extends Instance {
    
    const ROOT_DIR = __DIR__.'/../../../theme/not-found/';
    
    public function start() {
        $root = $this->getMain()->getParam(Util\getColonsPath(self::class).'@theme:root', self::ROOT_DIR);
        $page = new Page($this, $root);
        $part = parse_url('//'.$this->getRequestUri());
        try {
            return $page->run($part['path']);
        } catch (ResponseException $e) {
            throw $e;
        } catch (\Exception $e) {
            return $page->run('');
        }
    }
}
