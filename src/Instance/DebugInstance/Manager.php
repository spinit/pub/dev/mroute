<?php


namespace Spinit\Dev\MRoute\Instance\DebugInstance;

use Spinit\Dev\MRoute\Instance;

/**
 * Questa istanza viene caricata solo in fase di sviluppo per avviare l'installazione
 *
 * @author ermanno
 */
class Manager extends Instance {
    protected function start() {
    }
    
    public function install($user) {
    }
    public function init() {
        
    }
}
