<?php
use Spinit\Util;

function getArch($base, $name = '') 
{
    try {
        if (!$base->getDataSource($name)->getAdapter()) return '';
        return $base->getDataSource($name)->getAdapter()->getType();
    } catch (\Exception $e) {
        return '';
    }
}

function fspath() {
    $args = array_map(function($item) {
        return str_replace('/', DIRECTORY_SEPARATOR, rtrim($item, '/'));
    }, array_filter(func_get_args(), 'trim'));
    return implode(DIRECTORY_SEPARATOR, $args);
}
function loadConfig($object) 
{
    $args = func_get_args();
    array_shift($args);
    if (is_string($object)) {
        $info = pathinfo($object);
    } else {
        $rec = new \ReflectionObject($object);
        $info = pathinfo($rec->getFileName());
    }
    $par1 = array_shift($args);
    $fcnt = null;
    if (is_callable($par1)) {
        $fcnt = $par1;
        $ext = array_shift($args);
    } else {
        $ext = $par1;
    }
    $ext = $ext ?: 'xml';
    $cfile = $info['dirname'].'/'.$info['filename'].'.'.$ext;
    
    if (array_shift($args)) {
        debug($info, $cfile, is_file($cfile));
    }
    $cnt = file_get_contents($cfile);
    
    if ($fcnt) {
        $cnt = call_user_func($fcnt, $cnt);
    }
    $data = [];
                
    if (is_file($cfile)) {
        switch($ext) {
            case 'xml':
                $data = json_decode(json_encode(simplexml_load_string($cnt , null, LIBXML_NOCDATA)), 1);
                break;
            case 'json':
                $data = json_decode($cnt, 1);
                break;
        }
    }
    return new \Spinit\Util\DictionaryBase($data);
}

function debug()
{
    if (array_key_exists('REMOTE_ADDR', $_SERVER)) {
        if (!in_array(\Spinit\Util\getenv('DEBUG_IP'), [$_SERVER['REMOTE_ADDR'], '0.0.0.0'])) return;
    }
    if (strpos(Util\arrayGet($_SERVER,['argv',0]), 'phpunit') === false) {
        header('Content-type: text/plain');
    }
    $args = func_get_args();
    $e = new \Exception();
    for($i = 0; $i < 3; $i++) {
        $data = Spinit\Util\arrayGet($e->getTrace(), $i);
        if (!$data) {
            break;
        }
        if (key_exists('file', $data)) {
            echo '== '. $data['file'].':'.$data['line']." == \n";
        }
    }
    var_dump($args);
    exit;
}

function debugx()
{
    $args = func_get_args();
    $e = new \Exception();
    $data = $e->getTrace()[0];
    echo '== '. $data['file'].':'.$data['line']." == \n";
    var_dump($args);
    echo $e->getTraceAsString();
    exit;
}

function spinid($str)
{
    return strtoupper(md5($str));
}

function spinDoc($class)
{
    static $cache = [];
    $class = \Spinit\Util\getClassPath($class);
    if (!array_key_exists($class, $cache)) {
        $ref = new \ReflectionClass($class);
        //get the comment string
        $comment_string= $ref->getDocComment();

        //define the regular expression pattern to use for string matching
        $pattern = "/ @@([^ ]+) (.*)/";

        //perform the regular expression on the string provided
        preg_match_all($pattern, $comment_string, $matches);
        $data = [
            'id'=>trim(str_replace('\\', ':', $class), ':'),
            'class'=>$ref->getName(), 
            'ns'=>$ref->getNamespaceName(), 
            'name'=>$ref->getShortName(), 
            'label'=>$ref->getShortName(), 
            'file'=>$ref->getFileName()
        ];
        foreach($matches[1]?:[] as $k => $key) {
            $data[$key] = $matches[2][$k];
        }
        $cache[$class] = $data;
    }
    $args = func_get_args();
    array_shift($args);
    if (count($args)) {
        $list = [];
        foreach($args as $a) {
            $list[] = \Spinit\Util\arrayGet($cache[$class], $a);
        }
        return $list;
    }
    return $cache[$class];
}

function dateFormat($date, $inSep = '-', $type = 'date') {
    $part = explode(' ', $date);
    $tick = explode($inSep, $part[0]);
    if (count($tick)<2) {
        $tick = array_reverse(explode($inSep == '-' ? '/' : '-', $part[0]));
        $part[0] = implode($inSep, $tick);
    }
    return $type == 'date' ? $part[0] : implode(' ', $part);
}
