<?php

use Spinit\Util;
use Spinit\Lib\DataSource\AdapterManager;

// Impostazione ambiente di test
AdapterManager::addAdapter("test, test-inst", "Spinit:Dev:MRoute:Test:DataSourceAdapter");
Util\FactoryMap::set('test', 'Spinit\Dev\MRoute\Command\GetInstanceInfo', "Spinit:Dev:MRoute:Test:Command:GetInstanceInfo");
