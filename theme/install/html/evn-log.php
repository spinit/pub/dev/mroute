<?php
use Spinit\Dev\MRoute\Core\Channel;
use Spinit\Dev\MRoute\Console;
use Spinit\Util\Error\StopException;
use Spinit\Util;

$instance = $this->getInstance();
$channel = new Channel();
$channel
->add(function($stampa) use ($instance) {
    static $log = '';
    // recupera il log e invia se è diverso da quello precedentemente inviato
    $log2 = $instance->getConsole()->getLog();
    if ($log != $log2) {
        $log = $log2;
        $stampa($log, 'log');
    }
})
->add(function($stampa) use ($instance) {
    try {
        // se l'istanza corrente ha finito l'installazione ... chiede di effettuare il reload della pagina
        $info = $instance->getDataSource('main')->query('select stat from osx_ice where id = {{@id}}', ['id'=>$instance->getInfo('id')])->first();
        if (Util\arrayGet($info, 'stat') == '') {
            $stampa('location.reload()', 'exe');
            throw new StopException();
        }
    } catch (StopException $e) {
        throw $e;
    } catch (\Exception $e) {
        $stampa($e->getMessage().'ERROR', 'error');
        //$stampa('location.reload()', 'exe');
    }
})
->run();

