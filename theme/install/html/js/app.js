$(function() {
    var Desktop = {
        alert : function(message) {
            alert(message);
        }
    }
    $(document).ajaxStart(()=>{$('body').addClass('view-spinner')});
    $(document).ajaxStop(()=>{$('body').removeClass('view-spinner')});
    $('form').on('submit', function() {
        var post = $(this).serializeArray();
        post.push({'name':'_[osy][event]', 'value':'start'});
        $.ajax(location.href, {
            'method': 'POST',
            'data' : post
        }).then(function(response) {
            
            if (typeof(response) == typeof('')) {
                Desktop.alert(response);
                return;
            }
            response.message && Desktop.alert(response.message);
            
            response.data && response.data.exec && 
            $(response.data.exec).each((idx, item)=>{
                var code = item.shift();
                var cmd = new Function(code);
                cmd.apply(window, item);
            });
        }, function(xhr) {
            const response = xhr.responseJSON;
            response && response.message && Desktop.alert(response.message);
        });
        return false;
    });
});