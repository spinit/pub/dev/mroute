    <?php
    use Spinit\Util;
    /*
    header("Connection: close");
    ob_start();
    echo json_encode($this->getInstance()->getInfo('stat'));
    $size = ob_get_length();
    header("Content-Length: $size");
    ob_end_flush();
    flush();
    */
    if ($this->getInstance()->getInfo('stat.event')!='install') {
        return;
    }
    if ($this->getInstance()->getInfo('stat.pid') != '') {
        return;
    }
    $DS = $this->getInstance()->getDataSource('main');
    $console = $this->getInstance()->getConsole();

    $console->reset();
    $console->log('Avvio installazione');

    $console->status = 'init';

    register_shutdown_function(function () use ($console, $DS) {
        if ($console->status != 'end') {
            $DS->update('osx_ice', ['stat'=>['event'=>'install', 'stop'=>'error', 'step'=>$console->status]], $this->getInstance()->getInfo('id'));
        }
        $console->log('Installazione terminata : '.$console->status);
    });

    $admin = null;
    try {
        $DS->update('osx_ice', ['stat'=>['event'=>'install', 'pid'=> getmypid()]], $this->getInstance()->getInfo('id'));
        $rec = $DS->select('osx_ice',['id'=>$this->getInstance()->getInfo('id')], 'admin')->first();
        $admin = json_decode($rec['admin'], 1);
        $console->log('admin', $admin);
        $info = $this->getInstance()->getInfo();
        // vengono inizializzati tutti gli accessPoint impostati sull'istanza
        $cmd = '
            select hex(id) as id, manager, conf, url
            from osx_ice_url
            where id_ice = {{@id_ice}}
            and act = "1"
            -- Opensymap DEVE essere il primo manager da installare
            order by case when manager = "Spinit:Dev:Opensymap:Opensymap" then 0 else 1 end';
        $managerList = $DS->query($cmd,['id_ice'=>$this->getInstance()->getInfo('id')]);
        
        foreach($managerList as $acp) {
            $acp['id_url'] = $acp['id'];
            foreach(Util\asArray('home, name, con_str, id', ',') as $ff) {
                $acp[$ff] = $info[$ff];
            }
            $instance = $this->getInstance()->getMain()->makeInstance($acp);
            $instance->setRequest($this->getInstance()->getRequest());
            // e viene fatta partire l'installazione
            $console->log('Install manager : '.$acp['manager'].', url : '.$acp['url']);
            $instance->install($admin);
        }
        $console->status = 'end';
        $DS->update('osx_ice', ['stat'=>''], $this->getInstance()->getInfo('id'));
        $console->log('Installazione terminata');
        $DS->delete('osx_ice_url', ['id_ice' =>$this->getInstance()->getInfo('id'), 'manager'=>'Spinit:Dev:MRoute:Instance:DebugInstance:Manager'], null, true);
    } catch (\Exception $e) {
        $console->log('Error', $e->getMessage());
        return;
    }
