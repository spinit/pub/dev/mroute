<?php

function xinput($label, $name, $type, $value, $require) {
    $req = $require?'require="require"':'';
    $title = '';
    if ($label) {
        $title = '<label>'.$label.'</label>';
    }
    $snippet = <<< EOFSTR
        {$title}
        <input class="form-control" autocomplete="off" name="{$name}" value="{$value}" id="{$name}" type="{$type}" placeholder="{$label}" {$req} xdata-validation-required-message="Please enter your user name." />
        <p class="help-block text-danger"></p>
EOFSTR;

    return $snippet;
}
