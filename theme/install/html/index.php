<?php

require __DIR__.'/lib.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Freelancer - Start Bootstrap Theme</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="<?php echo $this->getUrl('assets/img/favicon.ico')?>" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<?php echo $this->getUrl('css/styles.css')?>" rel="stylesheet" />
        <link href="<?php echo $this->getUrl('css/app.css')?>" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <style>
            #spinner-command {
                display:none;
                position:fixed;
                z-index:10000;
                height: 100vh;
                width : 100%;
                background-color: #ceddef;
                opacity: 0.4;
                align-items: center;
                justify-content: center;
            }
            .view-spinner #spinner-command {
                display:flex; 
            }
        </style>
    </head>
    <body id="page-top">
        <div id="spinner-command">
            <div class="spinner-border">
            </div>
        </div>
        <?php
        if ($this->getInstance()->getInfo('id')) {
            require(dirname(__DIR__).'/page/console.php');
        } else {
            require(dirname(__DIR__).'/page/index.php');
        }
        ?>
    </body>
</html>
<?php
