<div>
    
    <div>Console : <?php echo get_class($this->getInstance())?></div>
    <pre id="console">
        
    </pre>
</div>
<script>
(function ($){
    // monitora il log
    var log = new EventSource('?_[osy][event]=log');
    log.onmessage = function(event) {
        console.log(event);
    }
    log.addEventListener('error', function(event) {
        console.error(event);
    });
    log.addEventListener('log', function(event) {
        $('#console').html(event.data);
    });
    log.addEventListener('exe', function(event) {
        (new Function(event.data))();
    });
})(jQuery);
</script>